package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bussiness.SDataCollector;
import com.bussiness.Student;

/**
 * Servlet implementation class DeleteData
 */
@WebServlet("/DeleteData")
public class DeleteData extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteData() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	String id =request.getParameter("id");
	System.out.println("id"+id);
	int Id=Integer.parseInt(id);
	SDataCollector dt= new SDataCollector();
	dt.doDelete(Id);
	PrintWriter out=	response.getWriter();
	out.println("Data Deleted Successfully!");
	
	SDataCollector dt1=new SDataCollector();
	ArrayList<Student> dataLst=dt1.doSelect();
	System.out.println("size::"+dataLst.size());
	request.setAttribute("student_info", dataLst);
RequestDispatcher rd=request.getRequestDispatcher("view.jsp");
rd.forward(request, response);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
