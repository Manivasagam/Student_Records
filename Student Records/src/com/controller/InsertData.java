package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bussiness.SDataCollector;
import com.bussiness.Student;

/**
 * Servlet implementation class InsertData
 */
@WebServlet("/InsertData")
public class InsertData extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertData() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		String Id = request.getParameter("id");
		String Name = request.getParameter("name");
		String Mark = request.getParameter("mark");
		int id=Integer.parseInt(Id);
		int mark=Integer.parseInt(Mark);
		
		System.out.println("id:"+id);
		System.out.println("name:"+Name);
		System.out.println("mark:"+mark);

		SDataCollector dt= new SDataCollector();
		int status=dt.doInsert(Id,Name,Mark);
		PrintWriter out=	response.getWriter();

		if(status>0)
		{
		out.println("record inserted suucessfully");
		SDataCollector dt1=new SDataCollector();
		ArrayList<Student> dataLst=dt1.doSelect();
		System.out.println("size::"+dataLst.size());
		request.setAttribute("student_info", dataLst);
	RequestDispatcher rd=request.getRequestDispatcher("view.jsp");
	rd.forward(request, response);
	}
		else{
			out.println("fails!!!");

		}
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
