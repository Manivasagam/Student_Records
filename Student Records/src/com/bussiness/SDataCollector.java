package com.bussiness;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

import com.mysql.jdbc.PreparedStatement;

public class SDataCollector {
	public Connection dbConnection()
	{
		Connection con=null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			 con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sys?useSSL=false","root","root");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return con;
	}
	
	public  ArrayList<Student> doSelect()
	{
		
		ArrayList<Student> dataLst=new ArrayList<Student>();
		try {
			Connection con=dbConnection();
			Statement stmt =  con.createStatement();
			ResultSet rs = stmt.executeQuery("Select * from student");
		while(rs.next())
		{

			int Id = rs.getInt(1);
			String Name = rs.getString(2);
			int Mark = rs.getInt(3);
			
			Student s=new Student();
			s.setId(Id);
			s.setName(Name);
			s.setMark(Mark);
			
			
			dataLst.add(s);
			System.out.println("::"+Id +":"+Name+":"+Mark);
		}
		con.close();
		
			
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dataLst;			

	}
	public int  doInsert(String id,String Name,String mark)
	{
		int resultStatus=-1;
		try {
			Connection con=dbConnection();
			Statement stmt =  con.createStatement();
			
			String query="insert into student values("+id+",'"+Name+"',"+mark+")";
			System.out.println("qry::"+query);
			 resultStatus=stmt.executeUpdate(query);
			System.out.println("status::"+resultStatus);

			}
			catch(Exception e)
			{
			System.out.print(e);
			e.printStackTrace();
			}
			
		
	      return resultStatus;
		
		}
	public void doDelete(int id) 
	{
		Connection con = dbConnection();
		try {
			Statement stmt =con.createStatement();
			int i=stmt.executeUpdate("DELETE FROM student WHERE id="+id);
			System.out.println("Data Deleted Successfully!");
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public void doUpdate(int Id,String name,int mark) throws SQLException
	{
		Connection con = dbConnection();
		Statement stmt = con.createStatement();
		String query = "Update student Set sname ='"+ name +"', mark="+mark+" Where id = "+Id;
		 int count = stmt.executeUpdate(query);
         System.out.println("Updated queries: "+count);
		
		
	}
	public ArrayList<Student> doUpdate1(int Idx)
	{
		ArrayList<Student>dataLst = new ArrayList<Student>();
		try
		{
		Connection con = dbConnection();
		String query = "Select * from student" +"WHERE id ="+Idx;
		System.out.println(query);
		Statement stmt = con.createStatement();
		ResultSet rs=stmt.executeQuery(query);
		while(rs.next())
		{
			int Id = rs.getInt(1);
			String Name = rs.getString(2);
			int Mark = rs.getInt(3);
			
			Student s=new Student();
			s.setId(Id);
			s.setName(Name);
			s.setMark(Mark);
			
			
			dataLst.add(s);
			System.out.println("::"+Id +":"+Name+":"+Mark);
		}
		con.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return dataLst;
	
		
	}
	public ArrayList<Student> doSort()
	{
		ArrayList<Student> dataLst=new ArrayList<Student>();
		try {
			Connection con=dbConnection();
			Statement stmt =  con.createStatement();

			String query="SELECT * from student order by id desc;";
			ResultSet rs=stmt.executeQuery(query);
		while(rs.next())
		{

			int Id = rs.getInt(1);
			String Name = rs.getString(2);
			int Mark = rs.getInt(3);
			
			Student s=new Student();
			s.setId(Id);
			s.setName(Name);
			s.setMark(Mark);
			
			
			dataLst.add(s);
			System.out.println("::"+Id +":"+Name+":"+Mark);
		}
		con.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dataLst;


	}
	}

	

